package me.mitkovic.android.java.myapplication;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.CellInfo;
import android.telephony.TelephonyManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {

    private int PERMISSION_ALL = 42;//;)
    private Executor requestCellUpdateExecutor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d("TELEPHONY", "MainActivity onCreate");

        requestPermissions();
    }

    @RequiresApi(29)
    public void requestCellInfoUpdate() {
        Log.d("TELEPHONY", "We have permissions. Continue");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q ) {
            TelephonyManager telephonyManager = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager != null) {
                try {
                    telephonyManager.requestCellInfoUpdate(requestCellUpdateExecutor, new TelephonyManager.CellInfoCallback() {
                        @Override
                        public void onCellInfo(@NonNull List<CellInfo> cellInfo) {
                            Log.d("TELEPHONY", "CellInfo Updated");
                        }

                        @Override
                        public void onError(int errorCode, @Nullable Throwable detail) {
                            super.onError(errorCode, detail);
                            Log.w("TELEPHONY", "Warning, cannot execute cellInfo update. Reason: " + (detail != null ? detail.getMessage() : ""));
                        }
                    });
                } catch (Exception e) {
                    Log.e("TELEPHONY", "ERROR Requesting Cell Update", e);
                }
            }
        }
    }

    private void requestPermissions() {
        Log.w("TELEPHONY", "PHONE and LOCATION permissions are not granted by default. Requesting permissions");

        String[] PERMISSIONS = {
                android.Manifest.permission.READ_PHONE_STATE,
                android.Manifest.permission.ACCESS_FINE_LOCATION
        };

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != PERMISSION_ALL) {
            Log.d("TELEPHONY", "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            Log.d("TELEPHONY", "PHONE permission granted");
            // we have permission
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) requestCellInfoUpdate();
        }

    }

    private boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}